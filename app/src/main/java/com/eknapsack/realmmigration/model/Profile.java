package com.eknapsack.realmmigration.model;

import io.realm.RealmObject;

/**
 * Created by IMTIAZ on 6/24/15.
 */
public class Profile extends RealmObject{
    private String name;
    private String info;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
