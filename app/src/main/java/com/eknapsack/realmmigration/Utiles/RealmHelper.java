package com.eknapsack.realmmigration.Utiles;

import android.content.Context;
import android.content.SharedPreferences;

import com.eknapsack.realmmigration.migration.Migration;

import java.io.File;

import io.realm.Realm;
import io.realm.RealmMigration;
import io.realm.exceptions.RealmMigrationNeededException;

/**
 * Created by progmaatic on 6/28/2015.
 *
 * http://stackoverflow.com/questions/29811940/how-do-i-get-the-realm-path-in-order-to-make-a-migration
 *
 */
public class RealmHelper {
    private static SharedPreferences prefs;
    public static Realm getInstance(Context context) {
        String realmPath = new File(context.getFilesDir(), "default.realm").getAbsolutePath();
        if (prefs.getBoolean("firstRun", true)) {
            Realm.migrateRealmAtPath(realmPath, new RealmMigration() {
                @Override
                public long execute(Realm realm, long version) {
                    return 3;
                }
            });
            prefs.edit().putBoolean("firstRun", false).commit();
        }

        try {
            return Realm.getInstance(context);
        } catch (RealmMigrationNeededException e) {
            Realm.migrateRealmAtPath(realmPath, new Migration());
            return Realm.getInstance(context);
        }
    }
}
