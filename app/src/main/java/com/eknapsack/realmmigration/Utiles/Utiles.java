package com.eknapsack.realmmigration.Utiles;

import android.content.Context;
import android.util.Log;

import com.eknapsack.realmmigration.migration.Migration;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by progmaatic on 6/24/2015.
 */
public class Utiles {

    public static Realm getRealmInstance(Context context) {
        Realm realm;
        try {
            realm = Realm.getInstance(context, "database.realm");
            Log.e("AAA ", "S "  +  "Realm ok");
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("AAA ", "S "  +  "Realm exception");
            RealmConfiguration config = new RealmConfiguration.Builder(context)
                    .name("database.realm")
                    .schemaVersion(1)
                    .migration(new Migration())
                    .build();

//                    .deleteRealmIfMigrationNeeded()

            realm = Realm.getInstance(config); // Automatically run migration if needed

//            realm.close();
        }

        return realm;
    }
}
