/*
 * Copyright 2014 Realm Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.eknapsack.realmmigration.migration;

import com.eknapsack.realmmigration.model.Profile;

import io.realm.Realm;
import io.realm.RealmMigration;
import io.realm.internal.ColumnType;
import io.realm.internal.Table;

/**
 * ************************** NOTE: *********************************************
 * The API for migration is currently using internal lower level classes that will
 * be replaced by a new API very soon! Until then you will have to explore and use
 * below example as inspiration.
 * ********************************************************************************
 */


public class Migration implements RealmMigration {
    @Override
    public long execute(Realm realm, long version) {

        /*
            // Version 0
            class Person
                String fullName;
                String lastName;
                int    age;

            // Version 1
            class Person
                String fullName;        // combine firstName and lastName into single field
                int age;
        */

        // Migrate from version 0 to version 1
        if (version == 0) {
            Table profileTable = realm.getTable(Profile.class);

            long nameIndex = getIndexForProperty(profileTable, "name");
//            long ageIndex = getIndexForProperty(profileTable, "age");
            long info = profileTable.addColumn(ColumnType.STRING, "info");
            for (int i = 0; i < profileTable.size(); i++) {
                profileTable.setString(info, i, profileTable.getString(nameIndex, i) + " AA");
            }
//            profileTable.removeColumn(getIndexForProperty(profileTable, "age"));
            version++;
        }

        return version;
    }

    private long getIndexForProperty(Table table, String name) {
        for (int i = 0; i < table.getColumnCount(); i++) {
            if (table.getColumnName(i).equals(name)) {
                return i;
            }
        }
        return -1;
    }
}
