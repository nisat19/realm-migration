package com.eknapsack.realmmigration;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.TextView;

import com.eknapsack.realmmigration.Utiles.Utiles;
import com.eknapsack.realmmigration.model.Profile;

import io.realm.Realm;

/**
 * Created by progmaatic on 6/24/2015.
 */
public class AnotherActivity extends ActionBarActivity {
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        realm = Utiles.getRealmInstance(this);

        findViewById(R.id.btnClick).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Profile profile = new Profile();
                    profile.setName("Name SS");
                    profile.setInfo("Info SS");
                    realm.beginTransaction();
                    realm.copyToRealm(profile);
                    realm.commitTransaction();

                    showStatus(realm);
                } catch (Exception e) {
                    e.printStackTrace();
                    realm.cancelTransaction();
                }

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    private String realmString(Realm realm) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Profile person : realm.allObjects(Profile.class)) {
            stringBuilder.append(person.toString()).append("\n");
        }

        return (stringBuilder.length() == 0) ? "<empty>" : stringBuilder.toString();
    }

    private void showStatus(Realm realm) {
        showStatus(realmString(realm));
    }

    private void showStatus(String txt) {
        ((TextView) findViewById(R.id.txtResult)).setText(txt);
    }
}
